import React, { Component } from 'react';
import Root from './pages/Root';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { HashRouter as Router } from 'react-router-dom';
import store from './redux';
import history from './history';
import axios from 'axios';

class App extends Component { 
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Router>
            <Root />                     
          </Router>          
        </ConnectedRouter>                 
      </Provider>     
    );
  }
}

export default App;
