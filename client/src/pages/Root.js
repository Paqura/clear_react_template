import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Switch, Route} from 'react-router-dom';
import Navigation from '../components/navigation';
import MainPage from './Main';
import FavoritesPage from './Favorites';

class RootComponent extends Component {
  render() {
    return (
      <div className="app">        
        <Navigation/>
        <Switch>
          <Route exact path="/" component={MainPage}/>
          <Route exact path="/favorites" component={FavoritesPage}/>
        </Switch>
      </div>
    );
  }
}

export default RootComponent;
