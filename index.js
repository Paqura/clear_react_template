const express = require('express');

const app = express();
const port = process.env.PORT || 8080;

app.get('/api', (req, res) => {
  res.json({
    "name": "Slava",
    "surname":  "Popov"
  })
});

app.listen(port, () => console.log(`Server working on ${port} port`));